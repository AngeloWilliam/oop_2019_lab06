package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ARRAY_START_COMPONENTS = 1_000;
	private static final int ARRAY_END_COMPONENTS = 2_000;
	
	private static final int FIRST_ELEMENT = 0;
	
	private static final int READINGS = 1000;
	
	private static final long AFRICA_POPULATION = 1_110_635_000L;
	private static final long AMERICAS_POPULATION = 972_005_000L;
	private static final long ANTARTICA_POPULATION = 0L;
	private static final long ASIA_POPULATION = 4_298_723_000L;
	private static final long EUROPE_POPULATION = 742_452_000L;
	private static final long OCEANIA_POPULATION = 38_304_000L;
	
	
	private static final int ELEMS = 100_000;
    private static final int TO_MS = 1_000_000;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    /**
     * @param s
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> a = new ArrayList<>();
    	
    	for(int i = UseCollection.ARRAY_START_COMPONENTS; i < UseCollection.ARRAY_END_COMPONENTS; i++) {
    		a.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	LinkedList<Integer> b = new LinkedList<>(a);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
    	Integer i = a.get(UseCollection.FIRST_ELEMENT);
    	a.set(UseCollection.FIRST_ELEMENT, a.get(a.size()-1));
    	a.set(a.size()-1, i);
    	
    	System.out.println("Primo elemento: " + a.get(FIRST_ELEMENT) + " "
    						+ "Ultimo elemento: " + a.get(a.size()-1));
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	/*for (Integer integer : a) {
			System.out.println(integer + ", ");
		}*/
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	

    	long time = System.nanoTime();
    	
    	for (Integer i1 : new Range4(1,UseCollection.ELEMS)) {
    		a.add(0,i1);
		}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("ArrayList time analysis");
    	System.out.println("Element added: " + UseCollection.ELEMS);
    	System.out.println("Time required (ns): " + time +" ns");
    	System.out.println("Time require (ms): " + (time/UseCollection.TO_MS) + "ms");
    	
    	
    	
    	time = System.nanoTime();
    	
    	for (Integer i1 : new Range4(1, UseCollection.ELEMS)) {
			b.add(0, i1);
		}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("LinkedList time analysis");
    	System.out.println("Element added: " + UseCollection.ELEMS);
    	System.out.println("Time required (ns): " + time +" ns");
    	System.out.println("Time require (ms): " + (time/UseCollection.TO_MS) + "ms");

        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
    	int middle = a.size()/2;
    	
    	time = System.nanoTime();
    	
    	for (Integer i1 : new Range4(1, UseCollection.READINGS)) {
			a.get(middle);
		}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("ArrayList time analysis");
    	System.out.println("Element read: " + UseCollection.READINGS);
    	System.out.println("Time required (ns): " + time +" ns");
    	System.out.println("Time require (ms): " + (time/UseCollection.TO_MS) + "ms");
    	
    	
    	time = System.nanoTime();
    	
    	for (Integer i1 : new Range4(1, UseCollection.READINGS)) {
			b.get(middle);
		}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("LinkedList time analysis");
    	System.out.println("Element read: " + UseCollection.READINGS);
    	System.out.println("Time required (ns): " + time +" ns");
    	System.out.println("Time require (ms): " + (time/UseCollection.TO_MS) + "ms");
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	
    	Map<String, Long> world = new HashMap<>();
    	world.put("Africa", UseCollection.AFRICA_POPULATION);
    	world.put("Americas", UseCollection.AMERICAS_POPULATION);
    	world.put("Antartica", UseCollection.ANTARTICA_POPULATION);
    	world.put("Asia", UseCollection.ASIA_POPULATION);
    	world.put("Europe", UseCollection.EUROPE_POPULATION);
    	world.put("Oceania", UseCollection.OCEANIA_POPULATION);
        /*
         * 8) Compute the population of the world
         */
    	long worldPop = 0;
    	//world.forEach((k,v)->worldPop+=v);
    	
    	for (Map.Entry<String,Long> entry : world.entrySet()) {
			worldPop += entry.getValue();
		}
    	
    	System.out.println(worldPop);
    }
}


class Range4 implements Iterable<Integer>{
    
    private final int start;
    private final int stop;
    
    public Range4(final int start, final int stop){
    	this.start = start;
    	this.stop = stop;
    }
    
    public Iterator<Integer> iterator(){
    	return new Iterator<Integer>(){
            // Non ci può essere costruttore!
            private int current = start; // o anche Range4.this.start
            
            public Integer next(){
            	return this.current++;
            }
            public boolean hasNext(){
            	return this.current <= stop; // o anche Range4.this.stop
            }
            public void remove(){}
        }; // questo è il ; del return!!
    }
}
