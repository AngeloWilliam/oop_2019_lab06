/**
 * 
 */
package it.unibo.oop.lab.exception2;

/**
 * @author angel
 *
 */
public class TransactionsOverQuotaException extends Exception{
	
	public TransactionsOverQuotaException() {
		super();
	}
	
	public String toString() {
		return "Raggiunto il numero massimo di operazioni presso l'ATM";
	}
}
