/**
 * 
 */
package it.unibo.oop.lab.exception2;

/**
 * @author angel
 *
 */
public class MainBankAccount {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AccountHolder mike = new AccountHolder("Mike", "Onofrio", 1);
		AccountHolder nino = new AccountHolder("Nino", "Frassica", 2);
		
		BankAccountE mi = new StrictBankAccountE(mike.getUserID(),10000,10);
		BankAccountE ni = new StrictBankAccountE(nino.getUserID(),10000,10);
		
		mi.withdraw(mike.getUserID(), 11000);
	}

}
