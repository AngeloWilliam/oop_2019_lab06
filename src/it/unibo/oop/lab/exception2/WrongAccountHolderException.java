/**
 * 
 */
package it.unibo.oop.lab.exception2;

/**
 * @author angel
 *
 */
public class WrongAccountHolderException extends IllegalArgumentException{
	
	private int wrongUsrId;
	
	public WrongAccountHolderException(final int usrId) {
		super();
		this.wrongUsrId = usrId;
	}
	
	public String toString() {
		String errMess;
		errMess = "UsrId " + this.wrongUsrId + " is not correct.";
		return errMess;
	}
}
