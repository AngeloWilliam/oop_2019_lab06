/**
 * 
 */
package it.unibo.oop.lab.exception2;

/**
 * @author angel
 *
 */
public class NotEnoughFoundsException extends IllegalArgumentException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5879821082368621417L;
	private double quantity;
	
	public NotEnoughFoundsException(final double quantity) {
		super();
		this.quantity = quantity;		
	}
	
	public String toString() {
		String errMess;
		errMess = "You can't withdraw " + this.quantity + "$";
		return errMess;
	}
}
