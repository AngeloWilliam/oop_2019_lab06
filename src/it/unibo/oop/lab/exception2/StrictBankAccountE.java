package it.unibo.oop.lab.exception2;

/**
 * Class modeling a BankAccount with strict policies: getting money is allowed
 * only with enough founds, and there are also a limited number of free ATM
 * transaction (this number is provided as a input in the constructor).
 * 
 */
public class StrictBankAccountE implements BankAccountE {

    private final int usrID;
    private double balance;
    private int nTransactions;
    private final int nMaxATMTransactions;
    private static final double ATM_TRANSACTION_FEE = 1;
    private static final double MANAGEMENT_FEE = 5;
    private static final double TRANSACTION_FEE = 0.1;

    /**
     * 
     * @param usrID
     *            user id
     * @param balance
     *            initial balance
     * @param nMaxATMTransactions
     *            max no of ATM transactions allowed
     */
    public StrictBankAccountE(final int usrID, final double balance, final int nMaxATMTransactions) {
        this.usrID = usrID;
        this.balance = balance;
        this.nMaxATMTransactions = nMaxATMTransactions;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void deposit(final int usrID, final double amount) {
        try {
        	checkUser(usrID);
        	this.balance += amount;
            incTransactions();
		} catch (WrongAccountHolderException e) {
			System.out.println(e);
		}
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void withdraw(final int usrID, final double amount) {
        /*
    	if (checkUser(usrID) && isWithdrawAllowed(amount)) {
            this.balance -= amount;
            incTransactions();
        }
        */
        try {
        	checkUser(usrID);
        	isWithdrawAllowed(amount);
        	this.balance -= amount;
        	incTransactions();
		} catch (NotEnoughFoundsException e) {
			System.out.println(e);
		}
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void depositFromATM(final int usrID, final double amount) {
        if (nTransactions < nMaxATMTransactions) {
            this.deposit(usrID, amount - StrictBankAccountE.ATM_TRANSACTION_FEE);
            nTransactions++;
        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void withdrawFromATM(final int usrID, final double amount) {
        if (nTransactions < nMaxATMTransactions) {
            this.withdraw(usrID, amount + StrictBankAccountE.ATM_TRANSACTION_FEE);
        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    public double getBalance() {
        return this.balance;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public int getNTransactions() {
        return nTransactions;
    }

    /**
     * 
     * @param usrID
     *            id of the user related to these fees
     */
    public void computeManagementFees(final int usrID) {
        final double feeAmount = MANAGEMENT_FEE + (nTransactions * StrictBankAccountE.TRANSACTION_FEE);
        try {
        	checkUser(usrID);
        	isWithdrawAllowed(feeAmount);
        	this.balance -= MANAGEMENT_FEE + nTransactions * StrictBankAccountE.TRANSACTION_FEE;
        	this.nTransactions = 0;
		} catch (WrongAccountHolderException | NotEnoughFoundsException e) {
			System.out.println(e);
		}
        /*
        if (checkUser(usrID) && isWithdrawAllowed(feeAmount)) {
            balance -= MANAGEMENT_FEE + nTransactions * StrictBankAccountE.TRANSACTION_FEE;
            nTransactions = 0;
        }
        */
    }

    private void checkUser(final int id) {
    	if(this.usrID!=id) {
    		throw new WrongAccountHolderException(id);
    	}
        //return this.usrID == id;
    }

    private void isWithdrawAllowed(final double amount) {
        if (balance < amount) {
        	throw new NotEnoughFoundsException(amount);
        }
    }

    private void incTransactions() {
        nTransactions++;
    }
}
